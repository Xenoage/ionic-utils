
/**
 * Some useful functions for working with arrays.
 */

/**
 * Returns true, when the given two arrays are equal.
 */
export function areArraysEqual(a1: any[], a2: any[]) {
	if (a1.length != a2.length)
		return false
	for (let i = 0; i < a1.length; i++)
		if (a1[i] != a2[i])
			return false
	return true
}

/**
 * Converts the given object into a string array.
 */
export function toStringArray(obj: any): string[] {
	if (Array.isArray(obj)) {
		let ret: string[] = [];
		for (let o of obj)
			ret.push("" + o);
		return ret;
	}
	else {
		return ["" + obj];
	}
}

/**
 * Returns a random item from the given array.
 */
export function getRandomItem<T>(array: T[]): T {
	return array[Math.floor(Math.random() * array.length)]
}

/**
 * Returns the sum of the items of the given array,
 * where each item is transformed to a number with the given function.
 */
export function sumBy<T>(array: T[], f: (T) => number): number {
	return array.reduce((acc, it) => acc + f(it), 0)
}

/**
 * Returns the average value of the items of the given array,
 * where each item is transformed to a number with the given function.
 */
export function averageBy<T>(array: T[], f: (T) => number): number {
	return sumBy(array, f) / array.length
}