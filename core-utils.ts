
/**
 * Some useful functions.
 */
export class CoreUtils {

	/**
	 * Converts the given object into a string array.
	 * @deprecated Use ArrayUtils instead
	 */
	static toStringArray(obj: any) : string[] {
  	if (Array.isArray(obj)) {
  		let ret : string[] = [];
  		for (let o of obj)
  			ret.push(""+o);
  		return ret;
	  }
	  else {
  		return [""+obj];
	  }
  }

	/**
	 * Returns true, if the Cordova platform is available.
	 */
	static isCordova() : boolean {
		return !!window["cordova"];
  }

	/**
	 * Returns true, if the Cordova platform is Android.
	 */
	static isCordovaAndroid() : boolean {
		return CoreUtils.isCordova() && window["device"]["platform"] == "Android";
	}

	/**
	 * Returns true, if the Cordova platform is iOS.
	 */
	static isCordovaIOs() : boolean {
		return CoreUtils.isCordova() && window["device"]["platform"] == "iOS";
	}

	/**
	 * Returns true, if the Cordova platform is Windows.
	 */
	static isCordovaWindows() : boolean {
		return CoreUtils.isCordova() && window["device"]["platform"].startsWith("Win");
	}

}
