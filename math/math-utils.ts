/**
 * Some useful mathematical functions.
 */

/**
 * Returns min, if x < min, else x.
 */
export function clampMin(x: number, min: number): number {
	if (x < min)
		return min
	else
		return x
}

/**
 * Returns the greatest common divisor of the given numbers.
 */
export function gcd(n1: number, n2: number): number {
	if (n2 == 0)
		return n1;
	else
		return gcd(n2, n1 % n2);
}