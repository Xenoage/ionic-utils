/**
 * This class represents a fraction of two integer values.
 *
 * It can for example be used to represent durations.
 * When possible, the fraction is cancelled automatically.
 */
import { clampMin, gcd } from "./math-utils";

export class Fraction {

	/**
	 * Creates a new fraction with the given numerator and denominator.
	 */
	constructor(public numerator: number,
	            public denominator: number) {
		if (denominator == 0)
			throw new Error("Denominator may not be 0")
		//if fraction is negative, always the numerator is negative
		let absNum = Math.abs(numerator);
		let absDen = Math.abs(denominator);
		if (numerator < 0 || denominator < 0) {
			if (numerator < 0 && denominator < 0) {
				numerator = absNum
			}
			else {
				numerator = -1 * absNum
			}
			denominator = absDen
		}
		let thisGcd = clampMin(gcd(absNum, absDen), 1)
		this.numerator = numerator / thisGcd
		this.denominator = denominator / thisGcd
	}

	/**
	 * Adds the given Fraction to this one and returns the result.
	 */
	plus(fraction: Fraction): Fraction {
		if (fraction == null)
			return this
		let numerator = this.numerator * fraction.denominator + this.denominator * fraction.numerator
		let denominator = this.denominator * fraction.denominator
		return new Fraction(numerator, denominator)
	}

	/**
	 * Subtracts the given fraction from this one and returns the result..
	 */
	minus(fraction: Fraction): Fraction {
		if (fraction == null)
			return this
		let numerator = this.numerator * fraction.denominator - this.denominator * fraction.numerator
		let denominator = this.denominator * fraction.denominator
		return new Fraction(numerator, denominator)
	}

	/**
	 * Returns true, iff the given fraction has the same value as this one.
	 */
	equals(fraction: Fraction): boolean {
		return this.compareTo(fraction) == 0
	}

	/**
	 * Compares this fraction with the given one.
	 * @returns the value <code>0</code> if this fraction is
	 *    equal to the given one; -1 if this fraction is numerically less
	 *    than the given one; 1 if this fraction is numerically
	 *    greater than the given one.
	 */
	compareTo(fraction: Fraction): number {
		let compare = this.minus(fraction);
		if (compare.numerator < 0)
			return -1;
		else if (compare.numerator == 0)
			return 0;
		else
			return 1;
	}

	/**
	 * Returns this fraction as a number.
	 */
	toNumber(): number {
		return this.numerator / this.denominator
	}

	/**
	 * Returns this fraction as a String in the format "numerator/denominator", e.g. "3/4".
	 */
	toString(): string {
		return this.numerator + "/" + this.denominator
	}

}