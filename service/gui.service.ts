import {Injectable} from "@angular/core";
import {Alert, AlertController, NavController} from "ionic-angular";

/**
 * Additional GUI features like alert boxes.
 */
@Injectable()
export class GuiService {

	private nav: NavController;

	constructor(public alertController : AlertController) {
	}

	init(nav: NavController) {
		this.nav = nav;
	}

	/**
	 * Shows an alert with the given message and an OK button.
	 * After clicking OK, the alert is closed, but nothing happens.
	 */
	showAlert(title: string, message: string) : void {
		this.showAlertAndDo(title, message, () => {});
	}

	/**
	 * Shows an alert with the given message and an OK button.
	 * After clicking OK, the given page is opened (pushed to the stack).
	 */
	showAlertAndPush(title: string, message: string,
		page : any) : void
	{
		this.showAlertAndDo(title, message, () => { this.nav.push(page); });
	}

	/**
	 * Shows an alert with the given message and an OK button.
	 * After clicking OK, the previous page is opened (i.e. the current
	 * page is popped from the stack).
	 */
	showAlertAndPop(title: string, message: string) : void {
		this.showAlertAndDo(title, message, () => { this.nav.pop(); });
	}

	/**
	 * Shows an alert with the given message and an OK button.
	 * After clicking OK, the root page is opened (i.e. the whole stack is cleared).
	 */
	showAlertAndPopToRoot(title: string, message: string) : void {
		this.showAlertAndDo(title, message, () => { this.nav.popToRoot(); });
	}

	/**
	 * Shows an alert with the given message and an OK button.
	 * After clicking OK, the given code is performed.
	 */
	showAlertAndDo(title: string, message: string, handler: () => void) : void
	{
		let alert = this._createAlert(title, message);
		this._addButton(alert, 'OK', handler);
		this._showAlert(alert);
	}

	/**
	 * Shows an alert with the given question and yes and no buttons.
	 * After clicking a button, the corresponding code is performed.
	 */
	showAlertYesOrNo(title: string, question: string,
		yesHandler: () => void, noHandler: () => void = () => {}) : void
	{
		let alert = this._createAlert(title, question);
		this._addButton(alert, 'Ja', yesHandler);
		this._addButton(alert, 'Nein', noHandler);
		this._showAlert(alert);
	}

	/**
	 * Shows an alert with the given message and a text input field.
	 * Depending on the clicked button, the corresponding code is performed.
	 */
	showAlertTextInput(title: string, message: string, inputText: string,
	                   okHandler: (text: string) => void, cancelHandler: () => void = () => {}) : void {
		let alert = this._createAlert(title, message);
		alert.addInput({ name: 'text', value: inputText })
		this._addButton(alert, 'Abbrechen', cancelHandler, "cancel");
		alert.addButton({ text: "OK", handler: data => okHandler(data.text) });
		this._showAlert(alert);
	}

	/**
	 * Shows an alert with the given message and the given list of strings for selection of a single item.
	 * When an item was clicked, the selectHandler is called with the index of the selected item.
	 * When the alert is cancelled, the cancelHandler is called.
	 */
	showAlertSelect(title: string, message: string, items: string[],
	                selectHandler: (itemIndex: number) => void, cancelHandler: () => void = () => {}) : void {
		let alert = this._createAlert(title, message);
		for (let i = 0; i < items.length; i++)
			alert.addInput({type: 'radio', label: items[i], value: ''+i});
		this._addButton(alert, 'Abbrechen', cancelHandler, "cancel");
		alert.addButton({ text: 'OK', handler: value => selectHandler(+value) });
		this._showAlert(alert);
	}

	/**
	 * Creates an alert with the given title and message and returns it.
	 */
	private _createAlert(title: string, message: string) : Alert {
		let alert = this.alertController.create({
			title: title,
			subTitle: message
		});
		return alert;
	}

	/**
	 * Adds a button with the given text and action to the given alert.
	 */
	private _addButton(alert: Alert, text: string, handler: () => void, role: string = null) : void {
		alert.addButton({
			text: text,
			handler: handler,
			role: role
		});
	}

	/**
	 * Displays the given alert.
	 */
	private _showAlert(alert: Alert) : void {
		alert.present();
	}

}
