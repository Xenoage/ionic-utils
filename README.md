# Ionic-Utils
Shared code for [Ionic](https://ionicframework.com) projects

## Projects using Ionic-Utils
Currently the following projects are based on this library:
* Ruhe Sanft Trauerdruck
* [Zong! Ear Training](https://github.com/Xenoage/Zong-Ear-Training)


