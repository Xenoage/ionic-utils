/**
 * Some useful functions for working with XML.
 */
export class XmlUtils {

  /**
   * Gets the parent document of the given element.
   */
  static getDocument(element: Element) : Document {
    while (element != null) {
      if (element.parentNode instanceof Document)
        return <Document> element.parentNode;
      element = element.parentElement;
    }
  }

  /**
   * Creates and returns a new document with the given
   * root element.
   */
  static createDocument(rootElementName: string) {
    return (new DOMParser()).parseFromString(`<${rootElementName}/>`, 'text/xml');
  }

  /**
   * Gets the first direct child element of the given parent element
   * with the given name. If there is none, null is returned.
   */
  static getElementByName(parent: Element, name: string) : Element {
    var children = parent.childNodes;
    for (var i = 0; i < children.length; i++) {
      var child = children[i];
      if (child instanceof Element && child.nodeName == name)
        return <Element> child;
    }
    return null;
  }

  /**
   * Gets all direct child elements of the given parent element.
   * If there are none, an empty array is returned.
   */
  static getElements(parent: Element) : Element[] {
    var ret : Element[] = [];
    var children = parent.childNodes;
    for (var i = 0; i < children.length; i++) {
      var child = children[i];
      if (child instanceof Element)
        ret.push(<Element> child);
    }
    return ret;
  }

  /**
   * Gets all direct child elements of the given parent element
   * with the given name. If there are none, an empty array is returned.
   */
  static getElementsByName(parent: Element, name: string) : Element[] {
    var ret : Element[] = [];
    var children = parent.childNodes;
    for (var i = 0; i < children.length; i++) {
      var child = children[i];
      if (child instanceof Element && child.nodeName == name)
        ret.push(<Element> child);
    }
    return ret;
  }

  /**
   * Gets all grandchild elements with the given name of the
   * child element with the given name from the given parent element.
   * If the child element does not exist, or if there are no grandchild
   * elements, an empty array is returned.
   */
  static getChildElementsByName(parent: Element, childName: string, grandChildName: string) {
    var eChild = this.getElementByName(parent, childName);
    if (eChild == null)
      return [];
    return this.getElementsByName(eChild, grandChildName);
  }

  /**
   * Parses a boolean value from a string.
   */
  static readBoolean(value: string) : boolean {
    if (value == null)
      return false;
    return value.toLowerCase() == "true";
  }

  /**
   * Creates and adds an element with the given name to the given parent element.
   */
  static addElement(elementName: string, parentElement: Element) : Element {
    let xmlDoc = this.getDocument(parentElement);
    let newElement = xmlDoc.createElement(elementName);
    parentElement.appendChild(newElement);
    return newElement;
  }

}
